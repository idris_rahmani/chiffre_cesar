import unittest
from ChiffreCesar import chiffrer
from ChiffreCesar import dechiffrer
from ChiffreCesar import chiffrer_circulaire
from ChiffreCesar import dechiffrer_circulaire

"""
ci-dessous on test les fontion chiffrer et déchiffrer du fichier Chiffre de césar
"""
class TestChiffreCesarFunctions(unittest.TestCase):

    def test_chiffrer(self):
        self.assertEqual(chiffrer('j',8),'r')
        self.assertEqual(chiffrer(';',8),'C')
        self.assertEqual(chiffrer('0',8),'8')
        self.assertEqual(chiffrer('_',8),'g')
        self.assertEqual(chiffrer('Cesar',8),'Km{iz')
        self.assertEqual(chiffrer('Chien',13),'Puvr{')

    def test_dechiffrer(self):
        self.assertEqual(dechiffrer('1',8),')')
        self.assertEqual(dechiffrer('u',8),'m')
        self.assertEqual(dechiffrer('+',8),'#')
        self.assertEqual(dechiffrer('H',8),'@')
        self.assertEqual(dechiffrer('Cesar',8),';]kYj')
        self.assertEqual(dechiffrer('Km{iz',8),'Cesar')
        self.assertEqual(dechiffrer('Puvr{',13),'Chien')

    def test_chiffrer_circulaire(self):
        self.assertEqual(chiffrer_circulaire('a',3),'d')
        self.assertEqual(chiffrer_circulaire('j',7),'q')
        self.assertEqual(chiffrer_circulaire('e',1),'f')
        self.assertEqual(chiffrer_circulaire('z',3),'c')
        self.assertEqual(chiffrer_circulaire('Cesar',3),'Fhvdu')

    def test_dechiffrer_circulaire(self):
        self.assertEqual(dechiffrer_circulaire('a',3),'x')
        self.assertEqual(dechiffrer_circulaire('j',7),'c')
        self.assertEqual(dechiffrer_circulaire('e',1),'d')
        self.assertEqual(dechiffrer_circulaire('z',3),'w')
        self.assertEqual(dechiffrer_circulaire('Cesar',3),'Zbpxo')