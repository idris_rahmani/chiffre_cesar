"""
Dans cette partie du code se trouve la partie qui gère l'interface graphique
"""
from tkinter import *
from tkinter.ttk import *
from ChiffreCesar import chiffrer, dechiffrer

"""
Les deux procédures ci_dessous vont permettre de récuperer le code contenu dans un Entry
et de le chiffrer ou le dechiffrer à l'aide des fonctions qui ont été implémenter dans le
fichier ChiffreCesar
"""
def clic_btn_chiffrer():
    saisie=zone_de_saisie.get()
    decalage=int(saisie_decalage.get())
    retour_de_saisie=chiffrer(saisie,decalage)
    zone_de_retour.config(text=retour_de_saisie)

def clic_btn_dechiffrer():
    saisie=zone_de_saisie.get()
    decalage=int(saisie_decalage.get())
    retour_de_saisie=dechiffrer(saisie,decalage)
    zone_de_retour.config(text=retour_de_saisie)

"""
Ici on définit que notre frame sera composé de 4 lignes et de 2 colonnes
"""
frame=Tk()
frame.title("chiffre de César")
frame.rowconfigure(0,weight=1)
frame.rowconfigure(1,weight=1)
frame.rowconfigure(2,weight=1)
frame.rowconfigure(3,weight=1)
frame.rowconfigure(4,weight=1)
frame.rowconfigure(5,weight=1)
frame.rowconfigure(6,weight=1)
frame.columnconfigure(0,weight=1)
frame.columnconfigure(1,weight=1)

valeur = StringVar()
cle_decalage = StringVar()
cle_decalage=("0")

"""
Ce label sert juste à afficher un message en haut au centre de la fenêtre
"""
label1 = Label(frame,text="Saisir le texte que vous souhaitez chiffrer/déchiffrer: ")
label1.grid(row=0, column=0, columnspan=2)

"""
zone_de_saisie va servir à entrer le code que l'on souhaitra coder au decoder
dans la ligne juste en-dessous du label précédant
"""
zone_de_saisie = Entry(frame, textvariable=cle_decalage)
zone_de_saisie.grid(row=1,column=0,columnspan=2)

label3=Label(frame,text="Saisir le décalage :")
label3.grid(row=2,column=0,columnspan=2)

saisie_decalage = Entry(frame,textvariable=int)
saisie_decalage.grid(row=3,column=0,columnspan=2)

"""L'entier ci-dessous permet de définir le décalage lorsqu'on va chiffrer/dechiffrer"""
nb_decalage=saisie_decalage.get()

"""
ce bouton chiffrer sera à gauche de la troisième ligne et servira
à chiffrer ce qui est contenu dans zone_de_saisie en appelant la procédure clic_btn_chiffrer créer plus haut
"""
bouton_chiffrer = Button(frame,text="Chiffrer",command=clic_btn_chiffrer)
bouton_chiffrer.grid(row=4,column=0)

"""
ce bouton dechiffrer sera à droite de la troisième ligne et servira
à chiffrer ce qui est contenu dans zone_de_saisie en appelant la procédure clic_btn_dechiffrer créer plus haut
"""
bouton_dechiffrer = Button(frame,text="Déchiffrer",command=clic_btn_dechiffrer)
bouton_dechiffrer.grid(row=4,column=1)

"""
Ce label sert juste à afficher un message au centre de la 3eme ligne de la fenêtre
"""
label2 = Label(frame,text="Voici le resultat : ")
label2.grid(row=5,column=0,columnspan=2)

"""
zone de retour est un label qui contient le retour de nos procédures chiffrer/déchiffrer
"""
zone_de_retour= Label(frame)
zone_de_retour.grid(row=6,column=0,columnspan=2)

frame.mainloop()