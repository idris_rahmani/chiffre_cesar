"""
La fonction chiffrer prend en entrer un caractère et un décalage,
récupère la valeur de ce caractère dans la table
ascii et lui ajoute le décalage correspondant. Elle reconvertie ensuite
cette  valeur dans la table ascii pour obtenir un
nouveau caractère qu'elle va retourner à l'utilisateur
"""
def chiffrer(une_phrase,decalage):
    phrase_chiffree=""
    for caractere in une_phrase:
        valeur_ascii=ord(caractere)
        valeur_ascii=valeur_ascii+decalage
        caractere_chiffrer=chr(valeur_ascii)
        phrase_chiffree="".join([phrase_chiffree,caractere_chiffrer])
    return phrase_chiffree

"""
Comme pour la fonction chiffrer ecrite au-dessus,
La fonction déchiffrer va prendre en paramètre un
caractère et un décalage, sauf qu'elle enlèvera cette fois_ci le décalage à
sa valeur ascii pour retourner le caractère correspondant
"""
def dechiffrer(une_phrase,decalage):
    phrase_dechiffree=""
    for caractere in une_phrase:
        valeur_ascii=ord(caractere)
        valeur_ascii=valeur_ascii-decalage
        caractere_dechiffrer=chr(valeur_ascii)
        phrase_dechiffree="".join([phrase_dechiffree,caractere_dechiffrer])
    return phrase_dechiffree



def chiffrer_circulaire(une_phrase,decalage):
    import string
    alphabet_minuscule_origine = string.ascii_lowercase
    alphabet_majuscule_origine = string.ascii_uppercase
    phrase_dechiffree=""
    alphabet_majuscule_decale = alphabet_majuscule_origine[decalage:]+alphabet_majuscule_origine[:decalage]
    alphabet_minuscule_decale = alphabet_minuscule_origine[decalage:]+alphabet_minuscule_origine[:decalage]
    for caractere in une_phrase:
        if(caractere.islower()):
            valeur_ascii=alphabet_majuscule_origine.index(caractere)
            caractere_modif = alphabet_majuscule_decale.index[valeur_ascii]
            phrase_dechiffree="".join([phrase_dechiffree,caractere_modif])
        else:
            valeur_ascii=alphabet_minuscule_origine.index(caractere)
            caractere_modif = alphabet_minuscule_decale.index[valeur_ascii]
            phrase_dechiffree="".join([phrase_dechiffree,caractere_modif])
    return phrase_dechiffree

def dechiffrer_circulaire(une_phrase,decalage):
    import string
    alphabet_minuscule_origine = string.ascii_lowercase
    alphabet_majuscule_origine = string.ascii_uppercase
    phrase_chiffree=""
    alphabet_majuscule_decale = alphabet_majuscule_origine[:decalage]+alphabet_majuscule_origine[decalage:]
    alphabet_minuscule_decale = alphabet_minuscule_origine[:decalage]+alphabet_minuscule_origine[decalage:]
    for caractere in une_phrase:
        if(caractere.islower()):
            valeur_ascii=alphabet_majuscule_decale.index[caractere]
            caractere_modif = alphabet_majuscule_origine.index(valeur_ascii)
            phrase_chiffree="".join([phrase_chiffree,caractere_modif])
        else:
            valeur_ascii=alphabet_minuscule_decale.index[caractere]
            caractere_modif = alphabet_minuscule_origine.index(valeur_ascii)
            phrase_chiffree="".join([phrase_chiffree,caractere_modif])
    return phrase_chiffree
     